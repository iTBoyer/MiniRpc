## MiniRpc 一个简单的模仿dubbo的小项目。
### 注册中心：zookeeper
### Tcp：netty
### 序列化：Gson、Kryo
### 动态代理：jdk Proxy、cglib。
### 连接池：common-pool

#### 现在尚未完工，还有很多bug和凑合的地方，只能勉强跑通注册和远程调用。开发目的是用来加深一下对dubbo的设计思路的理解。
#### 有兴趣的小伙伴可以发邮件<3392642972@qq.com>一起加入进来。