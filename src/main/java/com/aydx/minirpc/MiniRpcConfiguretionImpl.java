package com.aydx.minirpc;

import com.aydx.minirpc.core.MiniRpcConfig;
import com.aydx.minirpc.core.spring.MiniRpcSpringBootConfigurtion;
import com.aydx.minirpc.core.proxy.BeanProxyFactory;
import com.aydx.minirpc.core.proxy.CglibBeanFactory;
import com.aydx.minirpc.core.serializable.KryoSerializableFactory;
import com.aydx.minirpc.core.serializable.SerializableFactory;
import com.aydx.minirpc.core.server.AnswerRemoteCall;
import com.aydx.minirpc.core.spring.SpringAnswerRemoteCall;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;


/**
 * Created by aydx on 2019/1/6.
 */
@Component
@Lazy(true)
public class MiniRpcConfiguretionImpl extends MiniRpcSpringBootConfigurtion  {
    private Logger logger = LoggerFactory.getLogger(MiniRpcConfiguretionImpl.class);

    @Override
    public String getPackageScan() {
        return "com.aydx";
    }

    @Override
    public BeanProxyFactory getBeanProxyFactory() {
        return new CglibBeanFactory();
    }

    @Override
    public AnswerRemoteCall getAnswerRemoteCall() {
        return new SpringAnswerRemoteCall(getContext());
    }

    @Override
    public SerializableFactory getSerializableFactory() {
        return new KryoSerializableFactory();
    }

    @Override
    public MiniRpcConfig getMiniRpcConfig() {
        logger.info("生成MinRpc配置");
        MiniRpcConfig miniRpcConfig=new MiniRpcConfig();
        miniRpcConfig.setSelfServiceIp("127.0.0.1");
        miniRpcConfig.setSelfServicePort(3302);
        miniRpcConfig.setRegisterAddress("127.0.0.1:2181");
        return miniRpcConfig;
    }

}
