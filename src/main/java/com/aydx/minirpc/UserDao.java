package com.aydx.minirpc;

import com.aydx.minirpc.core.proxy.RemoteCall;

@RemoteCall(group = "mygroup",version ="1.1")
public interface UserDao {
    String getUser(Integer id);
}
