package com.aydx.minirpc;

import com.aydx.minirpc.core.proxy.RemoteService;
import org.springframework.stereotype.Component;

@Component
@RemoteService(group = "mygroup",version ="1.1")
public class UserDaoImpl implements UserDao {
    @Override
    public String getUser(Integer userId) {
        return "你好"+userId+"我离你很远哦";
    }

}
