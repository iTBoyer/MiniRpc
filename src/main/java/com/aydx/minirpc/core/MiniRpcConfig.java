package com.aydx.minirpc.core;

/**
 * Created by aydx on 2019/1/6.
 * 配置
 */
public class MiniRpcConfig {
    private String selfServiceIp;
    private Integer selfServicePort;
    private String registerAddress;

    public String getSelfServiceIp() {
        return selfServiceIp;
    }

    public void setSelfServiceIp(String selfServiceIp) {
        this.selfServiceIp = selfServiceIp;
    }

    public Integer getSelfServicePort() {
        return selfServicePort;
    }

    public void setSelfServicePort(Integer selfServicePort) {
        this.selfServicePort = selfServicePort;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }
}
