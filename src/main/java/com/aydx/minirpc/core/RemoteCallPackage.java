package com.aydx.minirpc.core;

import java.io.Serializable;

/**
 * Created by aydx on 2019/1/6.
 * 远程调用的数据包
 *
 */
public class RemoteCallPackage implements Serializable{
    private String remoteIp;
    private String remotePort;
    private String registerName;
    private String registerIp;
    private String registerPort;
    private String group;
    private String version;
    private String interfaceName;
    private RpcMethod method;
    private Object[] args;
    private Object returnResult;

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(String remotePort) {
        this.remotePort = remotePort;
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public String getRegisterPort() {
        return registerPort;
    }

    public void setRegisterPort(String registerPort) {
        this.registerPort = registerPort;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getInterfaceName() {

        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public RpcMethod getMethod() {
        return method;
    }

    public void setMethod(RpcMethod method) {
        this.method = method;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public Object getReturnResult() {
        return returnResult;
    }

    public void setReturnResult(Object returnResult) {
        this.returnResult = returnResult;
    }
}
