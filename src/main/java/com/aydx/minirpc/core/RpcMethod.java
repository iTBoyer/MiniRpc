package com.aydx.minirpc.core;

import java.io.Serializable;

/**
 * 方法包装
 */
public class RpcMethod implements Serializable {
    private String[] argsClassName;
    private String method;
    private String returnTypeName;

    public String[] getArgsClassName() {
        return argsClassName;
    }

    public RpcMethod setArgsClassName(String[] argsClassName) {
        this.argsClassName = argsClassName;
        return this;
    }

    public String getMethod() {
        return method;
    }

    public RpcMethod setMethod(String method) {
        this.method = method;
        return this;
    }

    public String getReturnTypeName() {
        return returnTypeName;
    }

    public RpcMethod setReturnTypeName(String returnTypeName) {
        this.returnTypeName = returnTypeName;
        return this;
    }
}
