package com.aydx.minirpc.core.client;

import java.util.concurrent.CountDownLatch;

/**
 * netty反馈结果
 */
public class NettyResoponseFuture {
    private CountDownLatch countDownLatch=new CountDownLatch(1);
    private byte[] result=null;

    /**
     * 获取结果
     * @return
     * @throws InterruptedException
     */
    public byte[] get() throws InterruptedException {
        this.countDownLatch.await();
        return result;
    }

    /**
     * 写入结果
     * @param bytes
     */
    public void write(byte[] bytes){
        result=bytes;
        this.countDownLatch.countDown();;
    }
}
