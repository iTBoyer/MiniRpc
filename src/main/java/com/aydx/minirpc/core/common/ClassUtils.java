package com.aydx.minirpc.core.common;

/**
 * 跟class操作有关的工具列
 */
public class ClassUtils {
    /**
     * 根据class数组获取classname数组
     * @param clazzs
     * @return
     */
    public static String[] getNames(Class<?>[] clazzs){
        if(clazzs!=null && clazzs.length>0){
            String[] names=new String[clazzs.length];
            for(int i=0;i<clazzs.length;i++){
                names[i]=clazzs[i].getName();
            }
            return names;
        }
        return null;
    }
    /**
     * 根据classname数组，加载类
     */
    public static Class<?>[] forNames(String[] clazzNames) throws ClassNotFoundException {
        if(clazzNames!=null && clazzNames.length>0){
            Class[] names=new Class[clazzNames.length];
            for(int i=0;i<clazzNames.length;i++){
                names[i]=Class.forName(clazzNames[i]);
            }
            return names;
        }
        return null;
    }
}
