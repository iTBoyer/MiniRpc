package com.aydx.minirpc.core.common;


import com.google.gson.*;

import java.lang.reflect.Type;

/**
 * Created by aydx on 2019/1/6.
 */
public class JsonUtils {
    public static Gson gson;
    static{
        GsonBuilder builder=new GsonBuilder();
        builder.registerTypeAdapter(Integer.class, new JsonSerializer<Integer>() {
            @Override
            public JsonElement serialize(Integer src, Type type, JsonSerializationContext jsonSerializationContext) {
                if (src == src.intValue())
                    return new JsonPrimitive(src.intValue());
                return new JsonPrimitive(src);
            }
        });
        gson=builder.create();
    }
    public static String toJson(Object o){
        return gson.toJson(o);
    }
    public static <T> T fromJson(String json,Class<T> clazz){
        return gson.fromJson(json,clazz);
    }
}
