package com.aydx.minirpc.core.common;

public class NameUtils {
    public static String genName(String interfaceName,String group,String version){
        StringBuilder stringBuilder=new StringBuilder("service_");
        stringBuilder.append(interfaceName).append("_").append(group).append("_").append(version);
        return stringBuilder.toString();
    }
    //首字母转小写
    public static String toLowerCaseFirstOne(String s){
        if(Character.isLowerCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
    }
    //首字母转大写
    public static String toUpperCaseFirstOne(String s){
        if(Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }
}
