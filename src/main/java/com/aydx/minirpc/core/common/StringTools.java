package com.aydx.minirpc.core.common;

/**
 * Created by aydx on 2019/1/6.
 */
public class StringTools {
    public static boolean isEmpty(String str){
        if(str==null || str.trim().equals("")){
            return true;
        }else{
            return false;
        }
    }
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }
}
