package com.aydx.minirpc.core.proxy;

import com.aydx.minirpc.core.MiniRpcContext;

/**
 * 代理生成器
 */
public abstract class BeanProxyFactory {
    private MiniRpcContext miniRpcContext;



    public MiniRpcContext getMiniRpcContext() {
        return miniRpcContext;
    }

    public void setMiniRpcContext(MiniRpcContext miniRpcContext) {
        this.miniRpcContext = miniRpcContext;
    }

    public abstract <T> T createProxy(Class<T> clazz);
}
