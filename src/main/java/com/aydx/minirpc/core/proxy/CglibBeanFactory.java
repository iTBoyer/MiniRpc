package com.aydx.minirpc.core.proxy;


import net.sf.cglib.proxy.Enhancer;

/**
 * cglib 代理生成器
 */
public class CglibBeanFactory extends BeanProxyFactory {


    @Override
    public <T> T createProxy(Class<T> clazz) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(new CglibProxyMethodHandler(clazz,getMiniRpcContext()));
        return (T)enhancer.create();
    }
}
