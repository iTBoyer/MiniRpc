package com.aydx.minirpc.core.proxy;

import com.aydx.minirpc.core.MiniRpcContext;
import com.aydx.minirpc.core.RemoteCallPackage;
import com.aydx.minirpc.core.RpcMethod;
import com.aydx.minirpc.core.common.ClassUtils;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * cglib 代理方法拦截器
 */
public class CglibProxyMethodHandler extends ProxyMethodHandler implements MethodInterceptor {

    public CglibProxyMethodHandler(Class<?> interfaceClass, MiniRpcContext miniRpcContext) {
        super(interfaceClass, miniRpcContext);
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if(!method.getDeclaringClass().equals(getInterfaceClass())){
            return methodProxy.invokeSuper(o, args);
        }else {
            //获取注解参数值
            RemoteCall annotation=method.getDeclaringClass().getAnnotation(RemoteCall.class);
            String registeName=annotation.registerName();
            String group=annotation.group();
            String version=annotation.version();

            //构建远程访问包
            RemoteCallPackage remoteCallPackage=new RemoteCallPackage();
            remoteCallPackage.setInterfaceName(method.getDeclaringClass().getName());
            //
            remoteCallPackage.setArgs(args);
            RpcMethod method1=new RpcMethod().setMethod(method.getName());
            if(method.getReturnType()!=null){
                method1.setReturnTypeName(method.getReturnType().getTypeName());
            }
            method1.setArgsClassName(ClassUtils.getNames(method.getParameterTypes()));
            remoteCallPackage.setMethod(method1);
            //
            remoteCallPackage.setGroup(group);
            remoteCallPackage.setVersion(version);
            return getMiniRpcContext().getRemoteCallCenter().call(remoteCallPackage,method.getReturnType());
        }
    }
}
