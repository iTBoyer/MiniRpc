package com.aydx.minirpc.core.proxy;
import java.lang.reflect.Proxy;
/**
 * jdk proxy 代理生成器
 */
public class JdkProxyBeanFactory extends BeanProxyFactory {

    @Override
    public <T> T createProxy(Class<T> clazz) {
        return (T)Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new JdkProxyMethodHandler(clazz,getMiniRpcContext()));
    }
}
