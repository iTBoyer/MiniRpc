package com.aydx.minirpc.core.proxy;

import com.aydx.minirpc.core.register.NodeType;

import java.io.Serializable;

/**
 * Created by aydx on 2019/1/6.
 * 注册节点信息
 */
public class NodeInfo implements Serializable{
    private NodeType nodeType;
    private String protocol;
    private String ip;
    private Integer port;
    private String group;
    private String version;
    private String interfaceName;

    public String getInterfaceName() {
        return interfaceName;
    }

    public NodeInfo setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public NodeInfo setGroup(String group) {
        this.group = group;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public NodeInfo setVersion(String version) {
        this.version = version;
        return this;
    }

    public NodeType getNodeType() {
        return nodeType;
    }

    public NodeInfo setNodeType(NodeType nodeType) {
        this.nodeType = nodeType;
        return this;
    }

    public String getProtocol() {
        return protocol;
    }

    public NodeInfo setProtocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public NodeInfo setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public NodeInfo setPort(Integer port) {
        this.port = port;
        return this;
    }
}
