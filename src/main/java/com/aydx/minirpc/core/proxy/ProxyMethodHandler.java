package com.aydx.minirpc.core.proxy;

import com.aydx.minirpc.core.MiniRpcContext;

/**
 * 代理-方法拦截器通用接口
 */
public abstract class ProxyMethodHandler {
    private Class<?> interfaceClass;
    private MiniRpcContext miniRpcContext;

    public ProxyMethodHandler(Class<?> interfaceClass,MiniRpcContext miniRpcContext) {
        this.interfaceClass = interfaceClass;
        this.miniRpcContext=miniRpcContext;
    }

    public Class<?> getInterfaceClass() {
        return interfaceClass;
    }

    public MiniRpcContext getMiniRpcContext() {
        return miniRpcContext;
    }
}
