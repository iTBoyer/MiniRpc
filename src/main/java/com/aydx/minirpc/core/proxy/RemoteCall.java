package com.aydx.minirpc.core.proxy;

import java.lang.annotation.*;

/**
 * Created by aydx on 2019/1/5.
 * 调用远程方法
 * 这里特么写错了，不应该是注册到bean，应该是用来注入。
 */
@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface RemoteCall {
    String group()  default "default";
    String version() default "1.0";
    String registerName() default "default";
}
