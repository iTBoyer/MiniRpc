package com.aydx.minirpc.core.proxy;

import java.lang.annotation.*;

/**
 * Created by aydx on 2019/1/5.
 * 提供远程服务
 */
@Documented
@Inherited
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface RemoteService {
    String group()  default "default";
    String version() default "1.0";
    String register() default "";
}
