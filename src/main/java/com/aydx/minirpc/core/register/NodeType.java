package com.aydx.minirpc.core.register;

/**
 * Created by aydx on 2019/1/6.
 */
public enum NodeType {
    PROVIDER(1,"provider")    ,
    CUSTOMER(2,"customer")    ;
    private int code;
    private String desc;

    NodeType(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
