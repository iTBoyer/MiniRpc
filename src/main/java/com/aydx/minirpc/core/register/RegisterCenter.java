package com.aydx.minirpc.core.register;

import com.aydx.minirpc.core.MiniRpcException;
import com.aydx.minirpc.core.common.StringTools;
import com.aydx.minirpc.core.proxy.NodeInfo;
import org.apache.zookeeper.KeeperException;
import org.jboss.netty.util.internal.ConcurrentHashMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 * 注册中心
 */
public class RegisterCenter {
    private ConcurrentMap<String, RpcRegister> registerMap;

    private RegisterCenter(ConcurrentMap<String, RpcRegister> registerMap) {
        this.registerMap=registerMap;
    }
    public static RegisterCenter newInstance(List<RpcRegister> rpcRegisterList){
        if(rpcRegisterList ==null || rpcRegisterList.size()==0){
            throw new MiniRpcException("注册中心不能为空");
        }
        ConcurrentMap<String, RpcRegister> registerMap2=new ConcurrentHashMap<>();
        for(RpcRegister register: rpcRegisterList){
            if(StringTools.isEmpty(register.getName())){
                throw new MiniRpcException("MiniRpc register.name 不能为空");
            }
            if(registerMap2.containsKey(register.getName())){
                throw new MiniRpcException("MiniRpc register.name 重复了");
            }
            registerMap2.put(register.getName(),register);
        }
        return new RegisterCenter(registerMap2);
    }
    /**
     * 查询节点
     */
    public List<NodeInfo> getNodes(String registerName,NodeType nodeType, String interfaceName, String group, String version) throws KeeperException, InterruptedException {
        if(registerMap==null || registerMap.size()==0){
            throw new MiniRpcException("没有任何注册器");
        }
        List<NodeInfo> nodeInfoList=new ArrayList<>();
        if(StringTools.isEmpty(registerName)){
            //registerName为空时，查询所有注册器
            Iterator<Map.Entry<String, RpcRegister>> iterator=this.registerMap.entrySet().iterator();
            while(iterator.hasNext()){
                RpcRegister register=iterator.next().getValue();
                nodeInfoList.addAll(register.getNode(nodeType,interfaceName,group,version));
            }
        }else{
            RpcRegister register=this.registerMap.get(registerName);
            if(register==null){
                throw new MiniRpcException("找不到注册器"+registerName);
            }
            nodeInfoList=register.getNode(nodeType,interfaceName,group,version);
        }
        return nodeInfoList;
    }
    /**
     * 注册节点
     */
    public void register(String registerName,NodeInfo nodeInfo) throws Exception {
        if(registerMap==null || registerMap.size()==0){
            throw new MiniRpcException("注册节点时没有任何注册器");
        }
        if(StringTools.isEmpty(registerName)){
            //registerName为空时，注册到所有注册器
            Iterator<Map.Entry<String, RpcRegister>> iterator=this.registerMap.entrySet().iterator();
            while(iterator.hasNext()){
                RpcRegister register=iterator.next().getValue();
                register.register(nodeInfo);
            }
        }else{
            RpcRegister register=this.registerMap.get(registerName);
            if(register==null){
                throw new MiniRpcException("注册节点时找不到注册器"+registerName);
            }
            register.register(nodeInfo);
        }
    }
    /**
     * 增加注册器
     * @return
     */
    public void addRegister(RpcRegister rpcRegister){
        this.registerMap.put(rpcRegister.getName(),rpcRegister);
    }

    public ConcurrentMap<String, RpcRegister> getRegisterMap() {
        return registerMap;
    }

    public void setRegisterMap(ConcurrentMap<String, RpcRegister> registerMap) {
        this.registerMap = registerMap;
    }
}
