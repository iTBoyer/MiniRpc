package com.aydx.minirpc.core.register;


import com.aydx.minirpc.core.proxy.NodeInfo;
import org.apache.zookeeper.KeeperException;

import java.util.List;

/**
 * Created by aydx on 2019/1/6.
 * 注册管理接口类
 */
public abstract class RpcRegister {
    public static String MINI_RPC_ROOT_NODE="/mini-rpc";
    private String name;
    /**
     * 注册一个节点
     */
    public abstract void register(NodeInfo nodeInfo) throws Exception;
    /**
     * 获取一个节点
     */
    public abstract List<NodeInfo> getNode(NodeType nodeType, String interfaceName, String group, String version) throws InterruptedException, KeeperException, KeeperException;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
