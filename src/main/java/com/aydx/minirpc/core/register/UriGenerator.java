package com.aydx.minirpc.core.register;

import com.aydx.minirpc.core.proxy.NodeInfo;
import com.aydx.minirpc.core.common.StringTools;

/**
 * Created by aydx on 2019/1/6.
 * 路径生成器
 */
public class UriGenerator {
    public static String gen(NodeInfo nodeInfo){
        StringBuilder stringBuilder=new StringBuilder(ZookeeperRegister.MINI_RPC_ROOT_NODE);
        if(StringTools.isNotEmpty(nodeInfo.getInterfaceName())){
            stringBuilder.append("/").append(nodeInfo.getInterfaceName());
        }
        if(StringTools.isNotEmpty(nodeInfo.getGroup())){
            stringBuilder.append("/").append(nodeInfo.getGroup());
        }
        if(StringTools.isNotEmpty(nodeInfo.getVersion())){
            stringBuilder.append("/").append(nodeInfo.getVersion());
        }
        if(nodeInfo.getNodeType()!=null){
            stringBuilder.append("/").append(nodeInfo.getNodeType().getCode());
        }
//        if(StringTools.isNotEmpty(nodeInfo.getIp())){
//            stringBuilder.append("/").append(nodeInfo.getIp()).append(";").append(nodeInfo.getPort());
//        }
        return stringBuilder.toString();
    }
}
