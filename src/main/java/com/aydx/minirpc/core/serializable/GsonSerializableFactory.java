package com.aydx.minirpc.core.serializable;

import com.aydx.minirpc.core.common.JsonUtils;

/**
 * 基于gson的序列化和反序列化
 */
public class GsonSerializableFactory implements SerializableFactory {
    @Override
    public byte[] serialize(Object o) {
        return JsonUtils.toJson(o).getBytes();
    }

    @Override
    public <T> T derialize(byte[] bytes, Class<T> clazz) {
        return JsonUtils.fromJson(new String(bytes),clazz);
    }
}
