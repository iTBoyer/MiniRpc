package com.aydx.minirpc.core.serializable;


/**
 * 序列化接口类
 */
public interface SerializableFactory {
    /**
     * 序列化
     * @return
     */
    byte[] serialize(Object o) throws Exception;

    /**
     * 反序列化
     */
    <T> T derialize(byte[] bytes,Class<T> clazz);
}
