package com.aydx.minirpc.core.server;

import com.aydx.minirpc.core.MiniRpcContext;
import com.aydx.minirpc.core.RemoteCallPackage;

/**
 * 响应远程调用
 */
public abstract class AnswerRemoteCall {
    private MiniRpcContext rpcContext;



    public MiniRpcContext getRpcContext() {
        return rpcContext;
    }

    public void setRpcContext(MiniRpcContext rpcContext) {
        this.rpcContext = rpcContext;
    }

    public abstract byte[] execute(RemoteCallPackage callPackage);
}
