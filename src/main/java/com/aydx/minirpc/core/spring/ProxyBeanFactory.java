package com.aydx.minirpc.core.spring;


import org.springframework.beans.factory.FactoryBean;

/**
 * Created by aydx on 2019/1/8.
 */
public class ProxyBeanFactory implements FactoryBean {
    private Class<?> interfaceClass;
    private Object proxy;

    public Class<?> getInterfaceClass() {
        return interfaceClass;
    }

    public void setInterfaceClass(Class<?> interfaceClass) {
        this.interfaceClass = interfaceClass;
    }

    public Object getProxy() {
        return proxy;
    }

    public void setProxy(Object proxy) {
        this.proxy = proxy;
    }

    @Override
    public Object getObject() throws Exception {
        return proxy;
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
