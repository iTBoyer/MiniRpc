package com.aydx.minirpc.core.spring;

import com.aydx.minirpc.core.MiniRpcContext;
import org.springframework.beans.factory.FactoryBean;

public class RpcContextFactory implements FactoryBean {
    private MiniRpcContext rpcContext;


    public MiniRpcContext getRpcContext() {
        return rpcContext;
    }

    public void setRpcContext(MiniRpcContext rpcContext) {
        this.rpcContext = rpcContext;
    }

    @Override
    public Object getObject() throws Exception {
        return rpcContext;
    }

    @Override
    public Class<?> getObjectType() {
        return MiniRpcContext.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
