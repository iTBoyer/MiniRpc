package com.aydx.minirpc;

import com.aydx.minirpc.core.common.ZooKeeperUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.util.List;
import java.util.concurrent.CountDownLatch;


public class Main {
    private static CountDownLatch latch=new CountDownLatch(1);
    public static void main(String[] args) throws Exception {
        ZooKeeper zooKeeper=new ZooKeeper("127.0.0.1:2181", 500, new Watcher() {
            @Override
            public void process(WatchedEvent watchedEvent) {
                if(Event.KeeperState.SyncConnected==watchedEvent.getState()){
                    latch.countDown();
                }
            }
        });
        latch.await();
        String uri= "/mini-rpc/com.aydx.minirpc.UserDaoImpl/mygroup/1.1/1";
        ZooKeeperUtils.createZNode(uri,"",zooKeeper, CreateMode.PERSISTENT);
        //
        uri=uri+"/"+"127.0.0.1,3001";
        ZooKeeperUtils.createZNode(uri,"kljljljljl",zooKeeper,CreateMode.EPHEMERAL);
        System.out.println("@@@@@@@@@@@@@");
        String path="/mini-rpc/com.aydx.minirpc.UserDao/mygroup/1.1/1";
        List<String> childs=zooKeeper.getChildren(path,false);
        zooKeeper.close();
    }
}
