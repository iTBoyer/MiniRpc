package com.aydx.minirpc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MiniRpcApplicationTests implements ApplicationContextAware {
    private ApplicationContext applicationContext;
    @Autowired
    private UserDao userDao;
    @Test
    public void contextLoads() {
    }
    @Test
    public void testUserDao(){
        String userInfo=this.userDao.getUser(22588);
        System.out.println(userInfo);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }
}

